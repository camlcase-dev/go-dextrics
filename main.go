package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/go-dextrics/internal/server"
)

func main() {
	server, err := server.New()
	if err != nil {
		logrus.WithField("error", err.Error()).Fatal("Failed to construct a new server.")
	}

	err = server.Start()
	if err != nil {
		logrus.WithField("error", err.Error()).Fatal("Failed to start go-dextrics.")
	}
}
