module gitlab.com/camlcase-dev/go-dextrics

go 1.14

require (
	github.com/caarlos0/env/v6 v6.3.0
	github.com/go-playground/validator/v10 v10.4.0
	github.com/go-resty/resty/v2 v2.3.0
	github.com/goat-systems/go-tezos/v3 v3.0.0
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fastjson v1.6.1
	gitlab.com/camlcase-dev/indexter v0.0.0-20200928171737-27ab7d49d9bf // indirect
	google.golang.org/protobuf v1.23.0
)
