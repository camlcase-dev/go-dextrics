package workers

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/go-dextrics/internal/coingecko"
	"gitlab.com/camlcase-dev/go-dextrics/internal/tzkt"
)

type Value struct {
	TokenName            string
	TokenAmount          int
	XTZAmount            int
	EstimatedDollarValue float64
}

type VolumeMetrics struct {
	contract string
	ticker   string
	div      float64
	USD      prometheus.Gauge
}

type TotalVolumeWorker struct {
	contracts       []VolumeMetrics
	totalVolume     prometheus.Gauge
	tzktClient      *tzkt.Client
	coinGeckoClient *coingecko.Client
	interval        time.Duration
}

func NewTotalVolumeWorker(contracts []Contract, interval time.Duration, registry *prometheus.Registry) TotalVolumeWorker {
	vm := []VolumeMetrics{}
	for _, contract := range contracts {
		metrics := VolumeMetrics{
			contract: contract.Address,
			ticker:   contract.CoingeckoTicker,
			div:      contract.Div,
			USD: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: fmt.Sprintf("estimate_volume_usd_value_%s", contract.Address),
				Help: fmt.Sprintf("The estimated volume in USD for contract '%s'", contract.Address),
			}),
		}
		vm = append(vm, metrics)
		registry.Register(metrics.USD)
	}

	totalsGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "total_estimated_volume_usd",
		Help: "The total estimated volume in USD value",
	})

	registry.Register(totalsGauge)

	tv := TotalVolumeWorker{
		contracts:       vm,
		interval:        interval,
		totalVolume:     totalsGauge,
		coinGeckoClient: coingecko.New(),
		tzktClient:      tzkt.New(),
	}
	tv.start()
	return tv
}

func (t *TotalVolumeWorker) start() {
	logrus.WithField("scrape-interval", t.interval).Info("TotalVolumeWorker started.")
	go func() {
		ticker := time.NewTicker(t.interval)
		for range ticker.C {
			totalUSDValue := float64(0)
			for _, contract := range t.contracts {
				logrus.WithField("scrape-interval", t.interval).Error("TotalVolumeWorker refreshing.")
				transactions, err := t.tzktClient.GetTransactions(contract.contract)
				if err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error()}).Error("TotalVolumeWorker failed to get transactions from tzkt.")
				}

				if usdValue, err := contract.process(transactions, t.coinGeckoClient); err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error()}).Error("TotalVolumeWorker failed to process transactions from tzkt.")
				} else {
					totalUSDValue += usdValue
				}
			}
			t.totalVolume.Set(totalUSDValue)
		}
	}()
}

func (v *VolumeMetrics) process(transactions tzkt.Transactions, coinGeckoClient *coingecko.Client) (float64, error) {
	volume := float64(0)
	for _, transaction := range transactions {
		if strings.Contains(transaction.Parameters, "tokenToXTZ") {
			if estimatedUSD, err := v.updateTokenToXTZ(transaction, coinGeckoClient); err != nil {
				return 0, errors.Wrap(err, "failed to update token to XTZ")
			} else {
				volume += estimatedUSD
			}
		}

		if strings.Contains(transaction.Parameters, "xtzToToken") {
			if estimatedUSD, err := v.updateXTZToToken(transaction, coinGeckoClient); err != nil {
				return 0, errors.Wrap(err, "failed to update XTZ to Token")
			} else {
				volume += estimatedUSD
			}
		}

	}

	v.USD.Set(volume)
	return volume, nil
}

func (v *VolumeMetrics) updateTokenToXTZ(transaction tzkt.Transaction, coinGeckoClient *coingecko.Client) (float64, error) {
	logrus.WithFields(logrus.Fields{
		"contract":  v.contract,
		"op":        transaction.Hash,
		"timestamp": transaction.Timestamp,
	}).Info("TotalVolumeWorker processing tokenToXTZ Transaction")
	tokenAmount, xtzAmount, err := getTokenToXtz(transaction.Parameters)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse transaction")
	}
	logrus.WithFields(logrus.Fields{
		"contract":    v.contract,
		"op":          transaction.Hash,
		"tokenAmount": tokenAmount,
		"xtzAmount":   xtzAmount,
		"timestamp":   transaction.Timestamp,
	}).Info("TotalVolumeWorker processed tokenToXTZ Transaction")

	year, month, day := time.Now().Date()
	xtzPrice, err := coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err.Error()}).Error("TotalVolumeWorker failed to get the price for tezos.")
	}

	tokenPrice := float64(1)
	if v.ticker != "" {
		tokenPrice, err = coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), v.ticker)
		if err != nil {
			logrus.WithFields(logrus.Fields{"error": err.Error()}).Errorf("TotalVolumeWorker failed to get the price for '%s'.", v.ticker)
		}
	}

	estimatedUSDValue := float64(transaction.Amount)/1000000*xtzPrice + float64(tokenAmount)/v.div*tokenPrice

	logrus.WithFields(logrus.Fields{
		"contract":     v.contract,
		"op":           transaction.Hash,
		"pool-tokens":  tokenAmount,
		"xtz":          transaction.Amount,
		"usd-estimate": estimatedUSDValue,
		"timestamp":    transaction.Timestamp,
	}).Info("TotalVolumeWorker refreshed volume values for contract.")

	return estimatedUSDValue, nil
}

func (v *VolumeMetrics) updateXTZToToken(transaction tzkt.Transaction, coinGeckoClient *coingecko.Client) (float64, error) {
	logrus.WithFields(logrus.Fields{
		"contract":  v.contract,
		"op":        transaction.Hash,
		"timestamp": transaction.Timestamp,
	}).Info("TotalVolumeWorker processing xtzToToken Transaction")
	tokenAmount, err := getXTZToToken(transaction.Parameters)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse transaction")
	}
	logrus.WithFields(logrus.Fields{
		"contract":    v.contract,
		"op":          transaction.Hash,
		"tokenAmount": tokenAmount,
		"xtzAmount":   transaction.Amount,
		"timestamp":   transaction.Timestamp,
	}).Info("TotalVolumeWorker processed xtzToToken Transaction")

	year, month, day := time.Now().Date()
	xtzPrice, err := coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err.Error()}).Error("TotalVolumeWorker failed to get the price for tezos.")
	}

	tokenPrice := float64(1)
	if v.ticker != "" {
		tokenPrice, err = coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), v.ticker)
		if err != nil {
			logrus.WithFields(logrus.Fields{"error": err.Error()}).Errorf("TotalVolumeWorker failed to get the price for '%s'.", v.ticker)
		}
	}

	estimatedUSDValue := float64(transaction.Amount)/1000000*xtzPrice + float64(tokenAmount)/v.div*tokenPrice
	logrus.WithFields(logrus.Fields{
		"contract":     v.contract,
		"op":           transaction.Hash,
		"pool-tokens":  tokenAmount,
		"xtz":          transaction.Amount,
		"usd-estimate": estimatedUSDValue,
		"timestamp":    transaction.Timestamp,
	}).Info("TotalVolumeWorker refreshed volume values for contract.")

	return estimatedUSDValue, nil
}

func (t *TotalVolumeWorker) getPrice(token string) (float64, error) {
	year, month, day := time.Now().Date()
	return t.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), token)
}
