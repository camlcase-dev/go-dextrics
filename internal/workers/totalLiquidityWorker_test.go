package workers

import (
	"testing"

	"github.com/goat-systems/go-tezos/v3/rpc"
	"github.com/stretchr/testify/assert"
)

func Test_storage(t *testing.T) {
	r, err := rpc.New("https://mainnet-tezos.giganode.io")
	assert.Nil(t, err)

	head, err := r.Head()
	assert.Nil(t, err)

	storage, err := r.ContractStorage(rpc.ContractStorageInput{
		Blockhash: head.Hash,
		Contract:  "KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf",
	})
	assert.Nil(t, err)

	lt, err := getLiquidityTokens(string(storage))
	assert.Nil(t, err)
	assert.Equal(t, float64(53246060562), lt)

	tp, err := getTokenPool(string(storage))
	assert.Nil(t, err)
	assert.Equal(t, float64(1070485691), tp)

	xtz, err := getXTZ(string(storage))
	assert.Nil(t, err)
	assert.Equal(t, float64(54589006750), xtz)
}
