package workers

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fastjson"
	"gitlab.com/camlcase-dev/go-dextrics/internal/coingecko"
	"gitlab.com/camlcase-dev/go-dextrics/internal/tzkt"
)

type token string

const (
	TZBTC token = "bitcoin"
	USDTZ token = "usd"
)

type addLiquidityParameters struct {
	Entrypoint string `json:"entrypoint"`
	Value      struct {
		Prim string `json:"prim"`
		Args []struct {
			Prim string `json:"prim"`
			Args []struct {
				String string `json:"string,omitempty"`
				Int    string `json:"int,omitempty"`
			} `json:"args"`
		} `json:"args"`
	} `json:"value"`
}

type dailyInterval struct {
	year  int
	month time.Month
	day   int
}

type PairValue struct {
	XTZ            int
	Token          int
	EstimatedValue float64
}

type Daily struct {
	contract        string
	token           token
	div             float64
	tzktClient      *tzkt.Client
	coinGeckoClient *coingecko.Client
	period          time.Duration
	addLiquidity    *sync.Map
	removeLiquidity *sync.Map
	totalLiquidity  *sync.Map
	xtzToToken      *sync.Map
	tokenToXTZ      *sync.Map
	volume          *sync.Map
}

func NewDaily(contract string, token token) Daily {
	var div float64
	if token == TZBTC {
		div = 100000000
	} else {
		div = 1000000
	}

	d := Daily{
		contract:        contract,
		token:           token,
		div:             div,
		tzktClient:      tzkt.New(),
		coinGeckoClient: coingecko.New(),
		period:          time.Hour * 24,
		addLiquidity:    &sync.Map{},
		removeLiquidity: &sync.Map{},
		totalLiquidity:  &sync.Map{},
		xtzToToken:      &sync.Map{},
		tokenToXTZ:      &sync.Map{},
		volume:          &sync.Map{},
	}

	d.start()
	return d
}

func (d *Daily) GetTotalVolume() (PairValue, error) {
	var p PairValue

	year, month, day := time.Now().Date()
	xtzPrice, err := d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		return p, errors.Wrap(err, "failed to get the current price for 'tezos'")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "price": xtzPrice}).Info("Got XTZ Price")

	var tokenPrice float64
	if d.token != USDTZ {
		tokenPrice, err = d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), string(d.token))
		if err != nil {
			return p, errors.Wrapf(err, "failed to get the current price for '%s'", string(d.token))
		}
	} else {
		tokenPrice = 1
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "price": tokenPrice}).Info("Got Token Price")

	d.tokenToXTZ.Range(func(key, value interface{}) bool {
		v := value.(PairValue) // Only safe because we only have one type in the cache.
		p.XTZ += v.XTZ
		p.Token += v.Token
		return true
	})

	d.xtzToToken.Range(func(key, value interface{}) bool {
		v := value.(PairValue) // Only safe because we only have one type in the cache.
		p.XTZ += v.XTZ
		p.Token += v.Token
		return true
	})

	p.EstimatedValue = (float64(p.XTZ) / 1000000 * xtzPrice) + (float64(p.Token) / d.div * tokenPrice)

	return p, nil
}

func (d *Daily) GetTotalLiquidity() PairValue {
	var p PairValue
	d.totalLiquidity.Range(func(key, value interface{}) bool {
		v := value.(PairValue) // Only safe because we only have one type in the cache.
		p.EstimatedValue += v.EstimatedValue
		p.XTZ += v.XTZ
		p.Token += v.Token
		return true
	})

	return p
}

func (d *Daily) start() {
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Starting initialization")
	transactions, err := d.tzktClient.GetTransactions(d.contract)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err.Error(), "contract": d.contract}).Error("Failed to get transactions.")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Finished getting initial transactions")

	if err := d.splitByDay(transactions); err != nil {
		logrus.WithFields(logrus.Fields{"error": err.Error(), "contract": d.contract}).Error("Failed to process transactions")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Finished processing initial transactions")

	go func() {
		ticker := time.NewTicker(time.Minute * 10)
		for range ticker.C {
			logrus.WithField("contract", d.contract).Info("Refrshing metrics")
			transactions, err := d.tzktClient.GetTransactions(d.contract)
			if err != nil {
				logrus.WithField("error", err.Error()).Error("Starting metrics scraping with daily keys")
			}
			logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Finished getting transactions")

			if err := d.splitByDay(transactions); err != nil {
				logrus.WithFields(logrus.Fields{"error": err.Error(), "contract": d.contract}).Error("Failed to process transactions")
			}
			logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Finished processing transactions")
		}
	}()
}

func (d *Daily) splitByDay(transactions tzkt.Transactions) error {
	for _, t := range transactions {
		if strings.Contains(t.Parameters, "addLiquidity") {
			if err := d.updateAddLiquidity(t); err != nil {
				return errors.Wrap(err, "failed to update added and total liquidity")
			}
		}

		if strings.Contains(t.Parameters, "removeLiquidity") {
			if err := d.updateRemoveLiquidity(t); err != nil {
				return errors.Wrap(err, "failed to update removed and total liquidity")
			}
		}

		if strings.Contains(t.Parameters, "tokenToXTZ") {
			if err := d.updateTokenToXTZ(t); err != nil {
				return errors.Wrap(err, "failed to update token to XTZ")
			}
		}

		if strings.Contains(t.Parameters, "xtzToToken") {
			if err := d.updateXTZToToken(t); err != nil {
				return errors.Wrap(err, "failed to update XTZ to Token")
			}
		}
	}

	return nil
}

func (d *Daily) updateAddLiquidity(t tzkt.Transaction) error {
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating addLiquidity")
	year, month, day := t.Timestamp.Date()
	di := dailyInterval{
		year:  year,
		month: month,
		day:   day,
	}

	token, err := getAddedLiquidity(t.Parameters)
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating addLiquidity")

	xtzPrice, err := d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	var tokenPrice float64
	if d.token != USDTZ {
		tokenPrice, err = d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), string(d.token))
		if err != nil {
			return errors.Wrap(err, "failed to parse transaction")
		}
	} else {
		tokenPrice = 1
	}

	if l, ok := d.addLiquidity.Load(di); !ok {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Storing new addLiquidity")
		d.addLiquidity.Store(di, PairValue{
			XTZ:            t.Amount,
			Token:          token,
			EstimatedValue: (float64(t.Amount) / 1000000 * xtzPrice) + (float64(token) / d.div * tokenPrice),
		})
	} else {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating addLiquidity")
		al := l.(PairValue)
		al.XTZ = al.XTZ + t.Amount
		al.Token = al.Token + token
		al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
		d.addLiquidity.Store(di, al)
	}

	if l, ok := d.totalLiquidity.Load(di); !ok {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Storing new total Liquidity")
		d.totalLiquidity.Store(di, PairValue{
			XTZ:            t.Amount,
			Token:          token,
			EstimatedValue: (float64(t.Amount) / 1000000 * xtzPrice) + (float64(token) / d.div * tokenPrice),
		})
	} else {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating total Liquidity")
		al := l.(PairValue)
		al.XTZ = al.XTZ + t.Amount
		al.Token = al.Token + token
		al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
		d.totalLiquidity.Store(di, al)
	}

	return nil
}

func (d *Daily) updateRemoveLiquidity(t tzkt.Transaction) error {
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating removeLiquidity")
	year, month, day := t.Timestamp.Date()
	di := dailyInterval{
		year:  year,
		month: month,
		day:   day,
	}

	xtz, token, err := getRemovedLiquidity(t.Parameters)
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating removeLiquidity")

	xtzPrice, err := d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	var tokenPrice float64
	if d.token != USDTZ {
		tokenPrice, err = d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), string(d.token))
		if err != nil {
			return errors.Wrap(err, "failed to parse transaction")
		}
	} else {
		tokenPrice = 1
	}

	if l, ok := d.addLiquidity.Load(di); !ok {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Storing new removeLiquidity")
		d.removeLiquidity.Store(di, PairValue{
			XTZ:            xtz,
			Token:          token,
			EstimatedValue: (float64(t.Amount) / 1000000 * xtzPrice) + (float64(token) / d.div * tokenPrice),
		})
	} else {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating removeLiquidity")
		al := l.(PairValue)
		al.XTZ = al.XTZ + xtz
		al.Token = al.Token + token
		al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
		d.removeLiquidity.Store(di, al)
	}

	l, ok := d.totalLiquidity.Load(di)
	if !ok {
		return errors.Wrap(err, "cannot remove liquidity before it's added")
	}
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token, "xtz": t.Amount, "fa1.2": token}).Info("Updating total Liquidity")
	al := l.(PairValue)
	al.XTZ = al.XTZ - t.Amount
	al.Token = al.Token - token
	al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
	d.totalLiquidity.Store(di, al)

	return nil
}

func (d *Daily) updateTokenToXTZ(t tzkt.Transaction) error {
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating tokenToXTZ")
	year, month, day := t.Timestamp.Date()
	di := dailyInterval{
		year:  year,
		month: month,
		day:   day,
	}

	token, xtz, err := getTokenToXtz(t.Parameters)
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	xtzPrice, err := d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	var tokenPrice float64
	if d.token != USDTZ {
		tokenPrice, err = d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), string(d.token))
		if err != nil {
			return errors.Wrap(err, "failed to parse transaction")
		}
	} else {
		tokenPrice = 1
	}

	if l, ok := d.tokenToXTZ.Load(di); !ok {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("New tokenToXTZ")
		d.tokenToXTZ.Store(di, PairValue{
			XTZ:            xtz,
			Token:          token,
			EstimatedValue: (float64(xtz) / 1000000 * xtzPrice) + (float64(token) / d.div * tokenPrice),
		})
	} else {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating existing tokenToXTZ")
		al := l.(PairValue)
		al.XTZ = al.XTZ + xtz
		al.Token = al.Token + token
		al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
		d.tokenToXTZ.Store(di, al)
	}

	return nil
}

func (d *Daily) updateXTZToToken(t tzkt.Transaction) error {
	logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating xtzToToken")
	year, month, day := t.Timestamp.Date()
	di := dailyInterval{
		year:  year,
		month: month,
		day:   day,
	}

	token, err := getXTZToToken(t.Parameters)
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	xtzPrice, err := d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), "tezos")
	if err != nil {
		return errors.Wrap(err, "failed to parse transaction")
	}

	var tokenPrice float64
	if d.token != USDTZ {
		tokenPrice, err = d.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), string(d.token))
		if err != nil {
			return errors.Wrap(err, "failed to parse transaction")
		}
	} else {
		tokenPrice = 1
	}

	if l, ok := d.xtzToToken.Load(di); !ok {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("New xtzToToken")
		d.xtzToToken.Store(di, PairValue{
			XTZ:            t.Amount,
			Token:          token,
			EstimatedValue: (float64(t.Amount) / 1000000 * xtzPrice) + (float64(token) / d.div * tokenPrice),
		})
	} else {
		logrus.WithFields(logrus.Fields{"contract": d.contract, "token": d.token}).Info("Updating xtzToToken")
		al := l.(PairValue)
		al.XTZ = al.XTZ + t.Amount
		al.Token = al.Token + token
		al.EstimatedValue = (float64(al.XTZ) / 1000000 * xtzPrice) + (float64(al.Token) / d.div * tokenPrice)
		d.xtzToToken.Store(di, al)
	}
	return nil
}

func getAddedLiquidity(parameters string) (int, error) {
	var params addLiquidityParameters
	if err := json.Unmarshal([]byte(parameters), &params); err != nil {
		return 0, errors.Wrap(err, "failed to parse added token for liquidity")
	}

	if len(params.Value.Args) == 2 {
		if len(params.Value.Args[1].Args) >= 1 {
			i := params.Value.Args[1].Args[0].Int
			token, err := strconv.Atoi(i)
			if err != nil {
				return 0, errors.Wrap(err, "failed to parse added token for liquidity")
			}
			return token, nil
		}
	}

	return 0, errors.New("failed to parse added token for liquidity")
}

func getRemovedLiquidity(parameters string) (int, int, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(parameters)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	var xtz, token int

	if obj, err := micheline.Object(); err == nil {
		if obj := obj.Get("value"); obj != nil {
			if obj.Get("args") != nil {
				args, err := obj.Get("args").Array()
				if err != nil {
					return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
				}

				if obj, err := args[1].Object(); err == nil {
					if obj.Get("args") != nil {
						args, err := obj.Get("args").Array()
						if err != nil {
							return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
						}

						if i := args[0].Get("int"); i != nil {
							xtz, err = strconv.Atoi(strings.Trim(i.String(), "\""))
							if err != nil {
								return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
							}
						}

						if args[1].Get("args") != nil {
							args, err := args[1].Get("args").Array()
							if err != nil {
								return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
							}
							if len(args) >= 1 {
								if i := args[0].Get("int"); i != nil {
									token, err = strconv.Atoi(strings.Trim(i.String(), "\""))
									if err != nil {
										return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if xtz == 0 && token == 0 {
		return 0, 0, errors.New("failed to parse removed token and xtz for liquidity")
	}

	return xtz, token, nil
}

func getTokenToXtz(parameters string) (int, int, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(parameters)
	if err != nil {
		return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	var xtz, token int

	if obj, err := micheline.Object(); err == nil {
		if obj := obj.Get("value"); obj != nil {
			if obj.Get("args") != nil {
				args, err := obj.Get("args").Array()
				if err != nil {
					return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
				}

				if obj, err := args[1].Object(); err == nil {
					if obj.Get("args") != nil {
						args, err := obj.Get("args").Array()
						if err != nil {
							return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
						}

						if i := args[0].Get("int"); i != nil {
							token, err = strconv.Atoi(strings.Trim(i.String(), "\""))
							if err != nil {
								return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
							}
						}

						if args[1].Get("args") != nil {
							args, err := args[1].Get("args").Array()
							if err != nil {
								return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
							}
							if len(args) >= 1 {
								if i := args[0].Get("int"); i != nil {
									xtz, err = strconv.Atoi(strings.Trim(i.String(), "\""))
									if err != nil {
										return 0, 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if xtz == 0 && token == 0 {
		return 0, 0, errors.New("failed to parse removed token and xtz for liquidity")
	}

	return token, xtz, nil
}

func getXTZToToken(parameters string) (int, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(parameters)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	var token int
	if obj, err := micheline.Object(); err == nil {
		if obj := obj.Get("value"); obj != nil {
			args, err := obj.Get("args").Array()
			if err != nil {
				return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
			}

			if len(args) == 2 {
				if obj, err := args[1].Object(); err == nil {
					if obj.Get("args") != nil {
						args, err := args[1].Get("args").Array()
						if err != nil {
							return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
						}
						i := args[0].Get("int")
						token, err = strconv.Atoi(strings.Trim(i.String(), "\""))
						if err != nil {
							return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
						}
					}
				}
			}
		}
	}

	return token, nil
}
