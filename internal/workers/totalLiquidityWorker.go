package workers

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/goat-systems/go-tezos/v3/rpc"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fastjson"
	"gitlab.com/camlcase-dev/go-dextrics/internal/coingecko"
)

type Contract struct {
	Address         string
	CoingeckoTicker string
	Div             float64
}

type LiquidityMetrics struct {
	contract  string
	ticker    string
	div       float64
	USD       prometheus.Gauge
	lastUSD   float64
	Pool      prometheus.Gauge
	lastPool  float64
	Token     prometheus.Gauge
	lastToken float64
	XTZ       prometheus.Gauge
	lastXTZ   float64
}

type TotalLiquidityWorker struct {
	rpc             rpc.IFace
	contracts       []LiquidityMetrics
	lastTotal       float64
	totalLiquidity  prometheus.Gauge
	interval        time.Duration
	coinGeckoClient *coingecko.Client
}

func NewTotalLiquidityWorker(rpc rpc.IFace, contracts []Contract, interval time.Duration, registry *prometheus.Registry) TotalLiquidityWorker {
	lm := []LiquidityMetrics{}
	for _, contract := range contracts {
		metrics := LiquidityMetrics{
			contract: contract.Address,
			ticker:   contract.CoingeckoTicker,
			div:      contract.Div,
			USD: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: fmt.Sprintf("estimate_usd_liquidity_value_%s", contract.Address),
				Help: fmt.Sprintf("The estimated USD value of the liquidity pool for contract '%s'", contract.Address),
			}),
			Pool: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: fmt.Sprintf("liquidity_tokens_%s", contract.Address),
				Help: fmt.Sprintf("The amount of liquidity pool tokens for contract '%s'", contract.Address),
			}),
			Token: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: fmt.Sprintf("pool_tokens_%s", contract.Address),
				Help: fmt.Sprintf("The amount of tokens in the liquidity pool for contract '%s'", contract.Address),
			}),
			XTZ: prometheus.NewGauge(prometheus.GaugeOpts{
				Name: fmt.Sprintf("xtz_%s", contract.Address),
				Help: fmt.Sprintf("The amount of xtz in the liquidity pool for contract '%s'", contract.Address),
			}),
		}
		lm = append(lm, metrics)
		registry.Register(metrics.USD)
		registry.Register(metrics.Pool)
		registry.Register(metrics.Token)
		registry.Register(metrics.XTZ)
	}

	totalsGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "total_liquidity_usd",
		Help: "The estimated USD value of all liquidity pools",
	})

	registry.Register(totalsGauge)

	tl := TotalLiquidityWorker{
		rpc:             rpc,
		contracts:       lm,
		interval:        interval,
		totalLiquidity:  totalsGauge,
		coinGeckoClient: coingecko.New(),
	}
	tl.start()
	return tl
}

func (t *TotalLiquidityWorker) start() {
	logrus.WithField("scrape-interval", t.interval).Info("TotalLiquidityWorker started.")
	go func() {
		ticker := time.NewTicker(t.interval)
		for range ticker.C {
			logrus.WithField("scrape-interval", t.interval).Error("TotalLiquidityWorker refreshing.")
			head, err := t.rpc.Head()
			if err != nil {
				logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error()}).Error("TotalLiquidityWorker failed to get the head block.")
				continue
			}

			xtzPrice, err := t.getPrice("tezos")
			if err != nil {
				logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error()}).Error("TotalLiquidityWorker failed to get the price for tezos.")
			}

			var totalEstimatedUSDValue float64
			for _, contract := range t.contracts {
				storage, err := t.rpc.ContractStorage(rpc.ContractStorageInput{
					Blockhash: head.Hash,
					Contract:  contract.contract,
				})
				if err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error(), "contract": contract.contract}).Error("TotalLiquidityWorker failed to get storage for contract.")
					continue
				}

				var poolTokens, estimatedUSDValue, tokens, xtz float64
				if poolTokens, err = getLiquidityTokens(string(storage)); err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error(), "contract": contract.contract}).Error("TotalLiquidityWorker failed to parse storage for contract.")
					continue
				}
				if tokens, err = getTokenPool(string(storage)); err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error(), "contract": contract.contract}).Error("TotalLiquidityWorker failed to parse storage for contract.")
					continue
				}
				if xtz, err = getXTZ(string(storage)); err != nil {
					logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error(), "contract": contract.contract}).Error("TotalLiquidityWorker failed to parse storage for contract.")
					continue
				}

				tokenPrice := float64(1)
				if contract.ticker != "" {
					tokenPrice, err = t.getPrice(contract.ticker)
					if err != nil {
						logrus.WithFields(logrus.Fields{"scrape-interval": t.interval, "error": err.Error()}).Error("TotalLiquidityWorker failed to get the price for tezos.")
					}
				}

				estimatedUSDValue = xtz/1000000*xtzPrice + tokens/contract.div*tokenPrice
				logrus.WithFields(logrus.Fields{"value": estimatedUSDValue, "xtz": xtz / 1000000, "xtz-price": xtzPrice, "tokens": tokens / contract.div, "token-price": tokenPrice}).Error("DEBUGING PRICE.")

				contract.Token.Set(tokens)
				contract.USD.Set(estimatedUSDValue)
				contract.XTZ.Set(xtz)
				contract.Pool.Set(poolTokens)

				totalEstimatedUSDValue += estimatedUSDValue
				logrus.WithFields(logrus.Fields{
					"liquidity-tokens": poolTokens,
					"pool-tokens":      tokens,
					"xtz":              xtz,
					"usd-estimate":     estimatedUSDValue,
					"contract":         contract.contract,
				}).Error("TotalLiquidityWorker refreshed liquidity values for contract.")
			}

			logrus.WithFields(logrus.Fields{
				"usd-estimate": totalEstimatedUSDValue,
			}).Error("TotalLiquidityWorker refreshed total liquidity value.")
			t.totalLiquidity.Set(totalEstimatedUSDValue)
		}
	}()
}

func getXTZ(blob string) (float64, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(blob)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	if obj, err := micheline.Object(); err == nil {
		if args := obj.Get("args"); args != nil {
			if argsArray, err := args.Array(); err == nil {
				if len(argsArray) >= 2 {
					if args := argsArray[1].Get("args"); args != nil {
						if argsArray, err := args.Array(); err == nil {
							if len(argsArray) >= 2 {
								if args := argsArray[1].Get("args"); args != nil {
									if argsArray, err := args.Array(); err == nil {
										if len(argsArray) >= 2 {
											if args := argsArray[1].Get("args"); args != nil {
												if argsArray, err := args.Array(); err == nil {
													if len(argsArray) >= 2 {
														val := argsArray[1].Get("int")
														i, err := strconv.Atoi(strings.Trim(val.String(), "\""))
														if err != nil {
															return 0, errors.New("failed to parse token pool")
														}

														return float64(i), nil
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0, errors.New("failed to parse token pool")
}

func getTokenPool(blob string) (float64, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(blob)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	if obj, err := micheline.Object(); err == nil {
		if args := obj.Get("args"); args != nil {
			if argsArray, err := args.Array(); err == nil {
				if len(argsArray) >= 2 {
					if args := argsArray[1].Get("args"); args != nil {
						if argsArray, err := args.Array(); err == nil {
							if len(argsArray) >= 2 {
								if args := argsArray[1].Get("args"); args != nil {
									if argsArray, err := args.Array(); err == nil {
										if len(argsArray) >= 2 {
											if args := argsArray[1].Get("args"); args != nil {
												if argsArray, err := args.Array(); err == nil {
													if len(argsArray) >= 1 {
														val := argsArray[0].Get("int")
														i, err := strconv.Atoi(strings.Trim(val.String(), "\""))
														if err != nil {
															return 0, errors.New("failed to parse token pool")
														}

														return float64(i), nil
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0, errors.New("failed to parse token pool")
}

func getLiquidityTokens(blob string) (float64, error) {
	var p fastjson.Parser
	micheline, err := p.Parse(blob)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse removed token and xtz for liquidity")
	}

	if obj, err := micheline.Object(); err == nil {
		if args := obj.Get("args"); args != nil {
			if argsArray, err := args.Array(); err == nil {
				if len(argsArray) >= 2 {
					if args := argsArray[1].Get("args"); args != nil {
						if argsArray, err := args.Array(); err == nil {
							if len(argsArray) >= 1 {
								if args := argsArray[0].Get("args"); args != nil {
									if argsArray, err := args.Array(); err == nil {
										if len(argsArray) >= 2 {
											if args := argsArray[1].Get("args"); args != nil {
												if argsArray, err := args.Array(); err == nil {
													if len(argsArray) >= 2 {
														val := argsArray[1].Get("int")
														i, err := strconv.Atoi(strings.Trim(val.String(), "\""))
														if err != nil {
															return 0, errors.New("failed to parse liquidity pool tokens")
														}

														return float64(i), nil
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0, errors.New("failed to parse liquidity pool tokens")
}

func (t *TotalLiquidityWorker) getPrice(token string) (float64, error) {
	year, month, day := time.Now().Date()
	return t.coinGeckoClient.GetPrice(fmt.Sprintf("%d-%d-%d", day, month, year), token)
}
