package config

import (
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// Config contains app indexter specific configurations
type Config struct {
	ContractViewAddress string        `env:"DEXTRICS_TEZOS_API" validate:"required"`
	ScrapeInterval      time.Duration `env:"INDEXTER_CACHE_EXPIRATION" envDefault:"1m"`
}

// New loads enviroment variables into a Config struct
func New() (Config, error) {
	config := Config{}
	if err := env.Parse(&config); err != nil {
		return config, errors.Wrap(err, "failed to load enviroment variables")
	}

	err := validator.New().Struct(&config)
	if err != nil {
		return config, errors.Wrap(err, "configuration validation failed")
	}

	return config, nil
}
