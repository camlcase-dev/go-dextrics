package coingecko

import (
	"encoding/json"
	"fmt"

	"github.com/go-resty/resty/v2"
	"github.com/pkg/errors"
)

type price struct {
	ID           string `json:"id"`
	Symbol       string `json:"symbol"`
	Name         string `json:"name"`
	Localization struct {
		En   string `json:"en"`
		De   string `json:"de"`
		Es   string `json:"es"`
		Fr   string `json:"fr"`
		It   string `json:"it"`
		Pl   string `json:"pl"`
		Ro   string `json:"ro"`
		Hu   string `json:"hu"`
		Nl   string `json:"nl"`
		Pt   string `json:"pt"`
		Sv   string `json:"sv"`
		Vi   string `json:"vi"`
		Tr   string `json:"tr"`
		Ru   string `json:"ru"`
		Ja   string `json:"ja"`
		Zh   string `json:"zh"`
		ZhTw string `json:"zh-tw"`
		Ko   string `json:"ko"`
		Ar   string `json:"ar"`
		Th   string `json:"th"`
		ID   string `json:"id"`
	} `json:"localization"`
	Image struct {
		Thumb string `json:"thumb"`
		Small string `json:"small"`
	} `json:"image"`
	MarketData struct {
		CurrentPrice struct {
			Aed  float64 `json:"aed"`
			Ars  float64 `json:"ars"`
			Aud  float64 `json:"aud"`
			Bch  float64 `json:"bch"`
			Bdt  float64 `json:"bdt"`
			Bhd  float64 `json:"bhd"`
			Bmd  float64 `json:"bmd"`
			Bnb  float64 `json:"bnb"`
			Brl  float64 `json:"brl"`
			Btc  float64 `json:"btc"`
			Cad  float64 `json:"cad"`
			Chf  float64 `json:"chf"`
			Clp  float64 `json:"clp"`
			Cny  float64 `json:"cny"`
			Czk  float64 `json:"czk"`
			Dkk  float64 `json:"dkk"`
			Dot  float64 `json:"dot"`
			Eos  float64 `json:"eos"`
			Eth  float64 `json:"eth"`
			Eur  float64 `json:"eur"`
			Gbp  float64 `json:"gbp"`
			Hkd  float64 `json:"hkd"`
			Huf  float64 `json:"huf"`
			Idr  float64 `json:"idr"`
			Ils  float64 `json:"ils"`
			Inr  float64 `json:"inr"`
			Jpy  float64 `json:"jpy"`
			Krw  float64 `json:"krw"`
			Kwd  float64 `json:"kwd"`
			Lkr  float64 `json:"lkr"`
			Ltc  float64 `json:"ltc"`
			Mmk  float64 `json:"mmk"`
			Mxn  float64 `json:"mxn"`
			Myr  float64 `json:"myr"`
			Nok  float64 `json:"nok"`
			Nzd  float64 `json:"nzd"`
			Php  float64 `json:"php"`
			Pkr  float64 `json:"pkr"`
			Pln  float64 `json:"pln"`
			Rub  float64 `json:"rub"`
			Sar  float64 `json:"sar"`
			Sek  float64 `json:"sek"`
			Sgd  float64 `json:"sgd"`
			Thb  float64 `json:"thb"`
			Try  float64 `json:"try"`
			Twd  float64 `json:"twd"`
			Uah  float64 `json:"uah"`
			Usd  float64 `json:"usd"`
			Vef  float64 `json:"vef"`
			Vnd  float64 `json:"vnd"`
			Xag  float64 `json:"xag"`
			Xau  float64 `json:"xau"`
			Xdr  float64 `json:"xdr"`
			Xlm  float64 `json:"xlm"`
			Xrp  float64 `json:"xrp"`
			Yfi  float64 `json:"yfi"`
			Zar  float64 `json:"zar"`
			Link float64 `json:"link"`
		} `json:"current_price"`
		MarketCap struct {
			Aed  float64 `json:"aed"`
			Ars  float64 `json:"ars"`
			Aud  float64 `json:"aud"`
			Bch  float64 `json:"bch"`
			Bdt  float64 `json:"bdt"`
			Bhd  float64 `json:"bhd"`
			Bmd  float64 `json:"bmd"`
			Bnb  float64 `json:"bnb"`
			Brl  float64 `json:"brl"`
			Btc  float64 `json:"btc"`
			Cad  float64 `json:"cad"`
			Chf  float64 `json:"chf"`
			Clp  float64 `json:"clp"`
			Cny  float64 `json:"cny"`
			Czk  float64 `json:"czk"`
			Dkk  float64 `json:"dkk"`
			Dot  float64 `json:"dot"`
			Eos  float64 `json:"eos"`
			Eth  float64 `json:"eth"`
			Eur  float64 `json:"eur"`
			Gbp  float64 `json:"gbp"`
			Hkd  float64 `json:"hkd"`
			Huf  float64 `json:"huf"`
			Idr  float64 `json:"idr"`
			Ils  float64 `json:"ils"`
			Inr  float64 `json:"inr"`
			Jpy  float64 `json:"jpy"`
			Krw  float64 `json:"krw"`
			Kwd  float64 `json:"kwd"`
			Lkr  float64 `json:"lkr"`
			Ltc  float64 `json:"ltc"`
			Mmk  float64 `json:"mmk"`
			Mxn  float64 `json:"mxn"`
			Myr  float64 `json:"myr"`
			Nok  float64 `json:"nok"`
			Nzd  float64 `json:"nzd"`
			Php  float64 `json:"php"`
			Pkr  float64 `json:"pkr"`
			Pln  float64 `json:"pln"`
			Rub  float64 `json:"rub"`
			Sar  float64 `json:"sar"`
			Sek  float64 `json:"sek"`
			Sgd  float64 `json:"sgd"`
			Thb  float64 `json:"thb"`
			Try  float64 `json:"try"`
			Twd  float64 `json:"twd"`
			Uah  float64 `json:"uah"`
			Usd  float64 `json:"usd"`
			Vef  float64 `json:"vef"`
			Vnd  float64 `json:"vnd"`
			Xag  float64 `json:"xag"`
			Xau  float64 `json:"xau"`
			Xdr  float64 `json:"xdr"`
			Xlm  float64 `json:"xlm"`
			Xrp  float64 `json:"xrp"`
			Yfi  float64 `json:"yfi"`
			Zar  float64 `json:"zar"`
			Link float64 `json:"link"`
		} `json:"market_cap"`
		TotalVolume struct {
			Aed  float64 `json:"aed"`
			Ars  float64 `json:"ars"`
			Aud  float64 `json:"aud"`
			Bch  float64 `json:"bch"`
			Bdt  float64 `json:"bdt"`
			Bhd  float64 `json:"bhd"`
			Bmd  float64 `json:"bmd"`
			Bnb  float64 `json:"bnb"`
			Brl  float64 `json:"brl"`
			Btc  float64 `json:"btc"`
			Cad  float64 `json:"cad"`
			Chf  float64 `json:"chf"`
			Clp  float64 `json:"clp"`
			Cny  float64 `json:"cny"`
			Czk  float64 `json:"czk"`
			Dkk  float64 `json:"dkk"`
			Dot  float64 `json:"dot"`
			Eos  float64 `json:"eos"`
			Eth  float64 `json:"eth"`
			Eur  float64 `json:"eur"`
			Gbp  float64 `json:"gbp"`
			Hkd  float64 `json:"hkd"`
			Huf  float64 `json:"huf"`
			Idr  float64 `json:"idr"`
			Ils  float64 `json:"ils"`
			Inr  float64 `json:"inr"`
			Jpy  float64 `json:"jpy"`
			Krw  float64 `json:"krw"`
			Kwd  float64 `json:"kwd"`
			Lkr  float64 `json:"lkr"`
			Ltc  float64 `json:"ltc"`
			Mmk  float64 `json:"mmk"`
			Mxn  float64 `json:"mxn"`
			Myr  float64 `json:"myr"`
			Nok  float64 `json:"nok"`
			Nzd  float64 `json:"nzd"`
			Php  float64 `json:"php"`
			Pkr  float64 `json:"pkr"`
			Pln  float64 `json:"pln"`
			Rub  float64 `json:"rub"`
			Sar  float64 `json:"sar"`
			Sek  float64 `json:"sek"`
			Sgd  float64 `json:"sgd"`
			Thb  float64 `json:"thb"`
			Try  float64 `json:"try"`
			Twd  float64 `json:"twd"`
			Uah  float64 `json:"uah"`
			Usd  float64 `json:"usd"`
			Vef  float64 `json:"vef"`
			Vnd  float64 `json:"vnd"`
			Xag  float64 `json:"xag"`
			Xau  float64 `json:"xau"`
			Xdr  float64 `json:"xdr"`
			Xlm  float64 `json:"xlm"`
			Xrp  float64 `json:"xrp"`
			Yfi  float64 `json:"yfi"`
			Zar  float64 `json:"zar"`
			Link float64 `json:"link"`
		} `json:"total_volume"`
	} `json:"market_data"`
	CommunityData struct {
		FacebookLikes            interface{} `json:"facebook_likes"`
		TwitterFollowers         int         `json:"twitter_followers"`
		RedditAveragePosts48H    float64     `json:"reddit_average_posts_48h"`
		RedditAverageComments48H float64     `json:"reddit_average_comments_48h"`
		RedditSubscribers        int         `json:"reddit_subscribers"`
		RedditAccountsActive48H  string      `json:"reddit_accounts_active_48h"`
	} `json:"community_data"`
	DeveloperData struct {
		Forks                        interface{} `json:"forks"`
		Stars                        interface{} `json:"stars"`
		Subscribers                  interface{} `json:"subscribers"`
		TotalIssues                  interface{} `json:"total_issues"`
		ClosedIssues                 interface{} `json:"closed_issues"`
		PullRequestsMerged           interface{} `json:"pull_requests_merged"`
		PullRequestContributors      interface{} `json:"pull_request_contributors"`
		CodeAdditionsDeletions4Weeks struct {
			Additions interface{} `json:"additions"`
			Deletions interface{} `json:"deletions"`
		} `json:"code_additions_deletions_4_weeks"`
		CommitCount4Weeks interface{} `json:"commit_count_4_weeks"`
	} `json:"developer_data"`
	PublicInterestStats struct {
		AlexaRank   int         `json:"alexa_rank"`
		BingMatches interface{} `json:"bing_matches"`
	} `json:"public_interest_stats"`
}

type Client struct {
	rsty *resty.Client
}

func New() *Client {
	return &Client{
		rsty: resty.New(),
	}
}

func (c *Client) GetPrice(date string, token string) (float64, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.coingecko.com/api/v3/coins/%s/history?date=%s", token, date))
	if err != nil {
		return 0, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	var p price
	if err = json.Unmarshal(resp.Body(), &p); err != nil {
		return 0, errors.Wrap(err, "failed to get price for token")
	}

	return p.MarketData.CurrentPrice.Usd, nil
}
