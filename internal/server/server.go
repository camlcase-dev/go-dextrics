package server

import (
	"net/http"
	"time"

	"github.com/goat-systems/go-tezos/v3/rpc"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/go-dextrics/internal/workers"
)

type Server struct {
	liquidityWorker workers.TotalLiquidityWorker
	volumeWorker    workers.TotalVolumeWorker
	registry        *prometheus.Registry
}

// New returns a new fa12 server
func New() (*Server, error) {
	r, err := rpc.New("https://mainnet-tezos.giganode.io")
	if err != nil {
		return nil, errors.Wrap(err, "failed to start server")
	}

	registry := prometheus.NewRegistry()

	contracts := []workers.Contract{
		{
			Address:         "KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf",
			CoingeckoTicker: "bitcoin",
			Div:             float64(100000000),
		},
		{
			Address: "KT1Puc9St8wdNoGtLiD2WXaHbWU7styaxYhD",
			Div:     1000000,
		},
	}

	return &Server{
		liquidityWorker: workers.NewTotalLiquidityWorker(
			r,
			contracts,
			time.Minute,
			registry,
		),
		volumeWorker: workers.NewTotalVolumeWorker(
			contracts,
			time.Minute,
			registry,
		),
		registry: registry,
	}, nil
}

// Start starts the server
func (s *Server) Start() error {
	logrus.Info("Starting Dextrics on Port :8080")
	http.Handle("/metrics", promhttp.HandlerFor(s.registry, promhttp.HandlerOpts{}))
	return http.ListenAndServe(":8080", nil)
}
