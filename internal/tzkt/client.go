package tzkt

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/pkg/errors"
)

type Transactions []Transaction

type Transaction struct {
	Type      string    `json:"type"`
	ID        int       `json:"id"`
	Level     int       `json:"level"`
	Timestamp time.Time `json:"timestamp"`
	Block     string    `json:"block"`
	Hash      string    `json:"hash"`
	Counter   int       `json:"counter"`
	Initiator struct {
		Alias   string `json:"alias"`
		Address string `json:"address"`
	} `json:"initiator"`
	Sender struct {
		Alias   string `json:"alias"`
		Address string `json:"address"`
	} `json:"sender"`
	Nonce         int `json:"nonce"`
	GasLimit      int `json:"gasLimit"`
	GasUsed       int `json:"gasUsed"`
	StorageLimit  int `json:"storageLimit"`
	StorageUsed   int `json:"storageUsed"`
	BakerFee      int `json:"bakerFee"`
	StorageFee    int `json:"storageFee"`
	AllocationFee int `json:"allocationFee"`
	Target        struct {
		Alias   string `json:"alias"`
		Address string `json:"address"`
	} `json:"target"`
	Amount     int    `json:"amount"`
	Parameters string `json:"parameters"`
	Status     string `json:"status"`
	Errors     []struct {
		Type string `json:"type"`
	} `json:"errors"`
	HasInternals bool `json:"hasInternals"`
	Quote        struct {
		Btc int `json:"btc"`
		Eur int `json:"eur"`
		Usd int `json:"usd"`
		Cny int `json:"cny"`
		Jpy int `json:"jpy"`
		Krw int `json:"krw"`
	} `json:"quote"`
}

type Client struct {
	rsty *resty.Client
}

func New() *Client {
	return &Client{
		rsty: resty.New(),
	}
}

func (c Client) GetTransactions(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&limit=10000", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get transactions")
	}

	return transactions, nil
}

func (c Client) AddLiquidity(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&parameters.contains=\"addLiquidity\"", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	return transactions, nil
}

func (c Client) RemoveLiquidity(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&parameters.contains=\"removeLiquidity\"", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get removeLiquidity transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	return transactions, nil
}

func (c Client) XtzToToken(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&parameters.contains=\"xtzToToken\"", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get xtzToToken transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	return transactions, nil
}

func (c Client) TokenToXtz(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&parameters.contains=\"tokenToXtz\"", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tokenToXtz transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	return transactions, nil
}

func (c Client) TokenToToken(contract string) (Transactions, error) {
	resp, err := c.rsty.R().Get(fmt.Sprintf("https://api.tzkt.io/v1/operations/transactions?target=%s&parameters.contains=\"tokenToToken\"", contract))
	if err != nil {
		return nil, errors.Wrap(err, "failed to get tokenToXtz transactions")
	}

	var transactions Transactions
	if err = json.Unmarshal(resp.Body(), &transactions); err != nil {
		return nil, errors.Wrap(err, "failed to get addLiquidity transactions")
	}

	return transactions, nil
}
